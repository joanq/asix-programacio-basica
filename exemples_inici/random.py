import random

# entra 0 i 5
n = random.randrange(6)
# o bé
n = random.randint(0,5)

# entre 1 i 10
n = random.randrange(1, 11)
# o bé
n = random.randint(1,10)
