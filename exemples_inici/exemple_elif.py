'''
Demana un nombre a l'usuari i digues quina de les
següents és la primera condició que es compleix:

- major de 10
- múltiple de 3
- negatiu
- cap de les anteriors
'''

num = float(input("Introdueix un nombre: "))

if num > 10:
    print("És major de 10")
else:
    if num % 3 == 0:
        print("És múltiple de 3")
    else:
        if num < 0:
            print("És negatiu")
        else:
            print("Cap de les condicions")


if num > 10:
    print("És major de 10")
elif num % 3 == 0:
    print("És múltiple de 3")
elif num < 0:
    print("És negatiu")
else:
    print("Cap de les condicions")

'''
Demana un nombre a l'usuari i digues quines de les
següents condicions es compleixen:

- major de 10
- múltiple de 3
- negatiu
- cap de les anteriors
'''

num = float(input("Introdueix un nombre: "))
capdelesanteriors=True

if num > 10:
    print("És major de 10")
    capdelesanteriors=False
if num % 3 == 0:
    print("És múltiple de 3")
    capdelesanteriors=False
if num<0:
    print("És negatiu")
    capdelesanteriors=False

if capdelesanteriors==True:
    print("Cap de les condicions")

