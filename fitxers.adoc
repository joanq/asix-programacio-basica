= Fitxers
:doctype: article
:encoding: utf-8
:lang: ca
:numbered:
:ascii-ids:

== Fitxers de text

=== Lectura

- Llegir un fitxer de text sencer:

[source,python]
----
with open('fitxer.txt') as fitxer: # equivalent: open('fitxer.txt', 'rt')
    text=fitxer.read()
    print(text)
----

- Llegir un fitxer de text caràcter a caràcter:

[source,python]
----
with open('fitxer.txt') as fitxer:
    while lletra:=fitxer.read(1):
        print(lletra)
----

[NOTE]
====
L'operador := per poder fer assignacions dins d'una expressió està disponible
a python des de la versió 3.8.
====

[source,python]
----
with open('fitxer.txt') as fitxer:
    lletra=fitxer.read(1)
    if lletra:
        print(lletra)
    while lletra:
        lletra=fitxer.read(1)
        if lletra:
            print(lletra)
----

[source,python]
----
with open('fitxer.txt') as fitxer:
    while True:
        lletra=fitxer.read(1)
        if not lletra:
            break
        print(lletra)
----

- Llegir un fitxer de text línia a línia:

[source,python]
----
with open('fitxer.txt') as fitxer:
    while linia:=fitxer.readline():
        print(linia, end='')
----

[source,python]
----
with open('fitxer.txt') as fitxer:
    for linia in fitxer:
        print(linia, end='')
----

=== Excepcions

[source,python]
----
try:
    with open('fitxer_inexistent.txt') as fitxer:
        for linia in fitxer:
            print(linia, end='')
except IOError as error:
    print('No es pot obrir el fitxer:', error)
----

[source,python]
----
def demana_nombre():
    correcte=False
    while not correcte:
        try:
            n=float(input('Dóna\'m un nombre: '))
            print('El nombre és: ', n)
            correcte=True
        except ValueError as error:
            print('No era un nombre :(')
    return nombre

n=demana_nombre()
# Fer el que sigui amb n
----

=== Escriptura

[source,python]
----
try:
    with open('nou_fitxer.txt', 'w') as fitxer:
        fitxer.write('test\n')
except IOError as error:
    print('No es pot obrir el fitxer:', error)
----

[NOTE]
====
Obrint el fitxer amb "w" es crea si no existeix i se sobreescriu si ja existeix.
En canvi, amb "x" es crea si no existeix, però es produeix una excepció si ja
existeix.

Si s'obre amb "a", el fitxer es crea si no existeix i, si existeix, s'afegeix
el nou contingut a continuació del que ja hi havia.
====

[source,python]
----
linies=[
'Primera línia\n',
'Segona línia\n'
]

try:
    with open('nou_fitxer.txt', 'w') as fitxer:
        fitxer.writelines(linies)
except IOError as error:
    print('No es pot obrir el fitxer:', error)
----

== Treballar amb l'estructura de fitxers i directoris

Per treballar amb fitxer i directoris disposem principalment dels mòduls
`os` i `shutil` de Python.

link:https://docs.python.org/3/library/os.html#os-file-dir[Operacions de fitxers i directoris del mòdul 'os']

link:https://docs.python.org/3/library/os.path.html#module-os.path[Útils per rutes]

[TIP]
====
Totes les funcions del mòdul `os` utilitzen excepcions de tipus `OSError` per
notificar els errors.
====

=== Mòdul `os`

==== Operacions principals sobre directoris

- Mostrar el directori de treball actual: `os.getcwd()`.
- Canviar el directori de treball: `os.chdir(directori)`.
+
[source,python]
----
import os

print(os.getcwd())
os.chdir('..')
print(os.getcwd())
----
+
[source]
----
/home/joan
/home
----

- Crear un directori: `os.mkdir(directori)`.
- Crear un arbre de directoris: `os.makedirs(ruta/a/crear)`.
- Veure el contingut d'un directori: `os.listdir(directori)`.
- Eliminar un directori: `os.rmdir(directori)`. El directori ha d'estar buit.
- Eliminar un arbre de directoris: `os.removedirs(ruta/a/esborrar)`.

==== Operacions principals sobre fitxers

- Esborrar un fitxer: `os.remove(fitxer)`.
- Obtenir la mida del fitxer: `os.path.getsize(fitxer)`.
- Obtenir informació sobre el fitxer: `os.stat(fitxer)`. Amb això obtenim un
objecte amb múltiples atributs que inclouen informació del fitxer, com la seva
mida, el moment en què s'ha creat, el seu inode, el seu propietari, etc. Els
resultats exactes depenen del sistema operatiu. Veure:
link:https://docs.python.org/3/library/os.html#os.stat_result[os.stat_result].

==== Operacions sobre fitxers o directoris

- Canviar el nom d'un fitxer o directori: `os.rename(nom_vell, nom_nou)`.
- Veure si una ruta apunta a un fitxer o a un directori: `os.path.isfile(ruta)`
retorna True si la ruta es tracta d'un fitxer. `os.path.isdir(ruta)` retorna `True` si
la ruta correspon a un directori.
- Obtenir la ruta absoluta a partir d'una ruta relativa: `os.path.abspath(ruta)`.

==== Recórrer el contingut d'un directori

Amb `scandir`:

[source,python]
----
with os.scandir(directori) as iterador:
    for element in iterador:
        if not element.name.startswith('.') and element.is_file():
            print(element.name)
----

Els elements obtinguts amb un `scandir` tenen, entre altres, els següents
atributs i mètodes:

- `name`
- `path`
- `inode()`
- `is_dir()`
- `is_file()`

Amb `os.walk` recorrem tot l'arbre de directoris, recursivament, des del
directori inicial que indiquem:

[source,python]
----
for dirpath, dirnames, filenames in os.walk('/var/log'):
    # dirpath conté la ruta del directori que estem examinant
    print(f'Directori: {dirpath}')
    # dirnames és una llista dels noms dels subdirectoris que hi ha
    for dirname in dirnames:
        print(f'Subdirectori: {dirname}')
    # filenames és una llista dels noms dels fitxers que hi ha
    for filename in filenames:
        print(f'Fitxer: {filename}')
----

[source]
----
Directori: /var/log
Subdirectori: cups
Subdirectori: private
Subdirectori: unattended-upgrades
Subdirectori: speech-dispatcher
Subdirectori: journal
Subdirectori: lightdm
Subdirectori: apt
Subdirectori: hp
Subdirectori: exim4
Subdirectori: installer
Fitxer: alternatives.log.2.gz
Fitxer: debug.2.gz
Fitxer: Xorg.0.log
Fitxer: popularity-contest.3.gz
Fitxer: auth.log.1
Fitxer: alternatives.log
Fitxer: debug.3.gz
...
Directori: /var/log/installer
Subdirectori: cdebconf
Fitxer: Xorg.0.log
Fitxer: partman
Fitxer: lsb-release
Fitxer: hardware-summary
Fitxer: syslog
Fitxer: status
Directori: /var/log/installer/cdebconf
Fitxer: templates.dat
Fitxer: questions.dat
----

=== Mòdul `shutil`

- Copiar un fitxer: `shutil.copy(origen, destí)`. Si destí és un directori,
copia el fitxer dins del directori.
- Copiar un arbre de directoris: `shutil.copytree(origen, destí)`.
- Eliminar un arbre de directoris: `shutil.rmtree(directori)`. Esborra l'arbre
sencer, amb tot el que contingui.
- Moure un fitxer o directori: `shutil.move(origen, destí)`.

== Fitxer binaris

=== Mòdul `pickle`

Per treballar amb fitxers binaris a alt nivell podem utilitzar una llibreria
com per exemple link:https://docs.python.org/es/3/library/pickle.html[pickle].

Aquesta llibreria ens permet guardar i recuperar estructures completes de
python en molt poques línies de codi.

[WARNING]
====
No és una llibreria segura, en el sentit que si un usuari manipula fitxers
creats per `pickle`, pot aconseguir que el nostre programa executi codi
arbitrari.
====

[NOTE]
====
Els fitxers creats per `pickle` són per estructures pròpies de python. Això vol
dir que no hi haurà limitacions pel que fa a quines dades podem guardar, i no
caldrà fer transformacions prèvies, però que els fitxers seran difícilment
llegibles des d'altres llenguatges de programació.
====

El següent programa escriu una llista de paraules a un fitxer binari. La funció
`escriu` es pot utilitzar realment per guardar qualsevol dada, des d'un nombre
enter fins a una llista de diccionaris.

[source,python]
----
import pickle

def escriu(dades):
    with open('dades.bin', 'wb') as fitxer:
        pickle.dump(dades, fitxer)

var=['llista', 'de', 'coses']
escriu(var)
----

El següent programa recupera i mostra la informació guardada al programa
anterior:

[source,python]
----
import pickle

def llegeix():
    with open('dades.bin', 'rb') as fitxer:
        dades=pickle.load(fitxer)
    return dades

var=llegeix()
print(var)
----
