n=int(input('Nombre? '))

for fila in range(0, n):
    n_espais = n-fila-1
    
    for col in range(0, n_espais):
        print(' ', end='')

    n_nums = fila*2+1
    for col in range(0, n_nums//2+1):
        print(col+1, end='')
    for col in range(fila, 0, -1):
        print(col, end='')
    print()


for fila in range(0, n-1):
    n_espais = fila+1
    
    for col in range(0, n_espais):
        print(' ', end='')

    n_nums = 2*n-3-2*fila
    for col in range(0, n_nums//2+1):
        print(col+1, end='')
    for col in range(0, n_nums//2):
        print(n_nums//2-col, end='')
    print()
